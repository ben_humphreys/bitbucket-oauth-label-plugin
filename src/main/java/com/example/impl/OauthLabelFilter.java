package com.example.impl;

import com.atlassian.bitbucket.request.RequestContext;
import com.atlassian.oauth.util.RequestAnnotations;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class OauthLabelFilter implements Filter {

    private final RequestContext requestContext;

    public OauthLabelFilter(RequestContext requestContext) {
        this.requestContext = requestContext;
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        if (RequestAnnotations.isOAuthRequest(httpServletRequest)) {
            requestContext.addLabel("oauth:" + RequestAnnotations.getOAuthConsumerKey(httpServletRequest));
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}